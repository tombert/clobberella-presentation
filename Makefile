pdf: 
	pandoc -t beamer presentation.md -V theme:Singapore --highlight-style=zenburn -o presentation.pdf

html: 
	pandoc -t revealjs presentation.md --highlight-style=zenburn -o presentation.html
