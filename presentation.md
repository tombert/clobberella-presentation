% Distributed Hash Tables, Video, and Fun!
% Thomas Gebert and Nicholas Misturak
% November 22, 2019

# About Us

-   Thomas Gebert

    -   Software Engineer in New York, NY.
    -   Certified Eccentric.
    -   Mostly focuses on backend services.

-   Nicholas Misturak

    -   Front-End Web Developer in Orlando, FL
    -   **Not** a Clojure programmer
    -   Work in JavaScript and React

## Not Experts

-   We are hobbyists who want to build cool stuff.
-   Possible we are doing things sub-optimally
    -   If you see something we're doing wrong, let us know!

# Background

## We want to make a competitor to YouTube

-   YouTube has a near-monopoly on video-sharing on the web
-   As a central entity, they have the benefit of complete control of their platform
-   However, they also assume all of the cost of maintaining this platform
    -   Video transcoding and storage are incredibly expensive
    -   Completely infeasable to scale with the resources of most startups

## The Solution

-   Don't write a centralized system, distribute it
-   By distributing, we can ask participants in the platform to do the work of transcoding and storage
-   In this way, we are distributing the cost of the platform

## What we've built

![](high-level.png)

## What we've built

-   Every node on the system is a client _and_ a server.
-   Communication happens over TCP...even for the local client.

## Prototype

-   Original version built in early 2018 in Haskell.
-   Code was a mess
    -   Haskell's networking libraries are annoyingly primitive.
    -   Concurrency in Haskell is largely shared-memory and doesn't make a fun blog post on Hacker News.
-   Needed a rewrite in a language with better library support.

## Why Clojure?

-   Wanted Go's CSP in a functional language.
-   Needed mature and well-supported network libraries.
    -   Could not be vanilla Java because life is too short as it is.
-   Dynamic languages are nice for network applications.

## Stack

-   Chestnut
    -   lein-figwheel
    -   re-frame
    -   reagent
    -   http-kit
    -   compojure
    -   Component dependency injection.
-   Secretary routing.

## Libraries Used

-   JeroMQ
    -   Low-level socket library to handle TCP and UDP logic.
-   MessagePack
    -   Incredibly fast binary-serialization library with bindings in virtually every language.
-   core.async
    -   Everyone here knows what this is.

# Distributed Hash Tables

## What is a DHT?

-   A data structure to store data and routing information across a cluster of nodes without a centralized server.
-   Data is shared and replicated across nodes.
    -   There are no central servers.
-   Popular algorithms include Chord, Pastry, Tapestry, and Kademlia

## Kademlia

-   Most popular algorithm for distributed hash table programs
-   Used by Facebook and Blizzard to distribute updates.
-   Provides worst-case $\log_2\left(n\right)$ lookups

# Routing

## What is a routing table?

A routing table provides a way for us to look up and store nodes that are of interest to us.

## What's the point of a routing table?

-   It would be infeasible to store reference to all the nodes in the cluster.
-   Ideally, we want to store a small subset of all the nodes that will allow us to find any given value or node in the table eventually.
-   It would be best if these lookups were faster than linear time and were guaranteed to resolve.

## Routing Table

-   Each machine participating in the DHT is a Node.
-   Nodes are given random 160 bit IDs.
-   "Distance" is defined by XORing two IDs.
    -   Might seem weird to use as a distance metric but consider this.
        -   $a \oplus b = 0 \iff a = b$
        -   $a \oplus b = b \oplus a$
    -   Gives us a deterministic way to figure out path to where data is stored without central synchronization.
-   Each node has its own routing table.
-   The table is divided into buckets of a set size.
    -   For the sake of this demo, let's assume that the size is $5$.

## Routing Table

![](routing-table-1.png)

## Routing Table

![](routing-table-2.png)

## Routing Table

![](routing-table-3.png)

## Routing Table

![](routing-table-4.png)

## Routing Table

![](routing-table-5.png)

## Routing Table

![](routing-table-6.png)

## Routing Table

![](routing-table-7.png)

## Routing Table

![](routing-table-8.png)

## Routing Table

![](routing-table-9.png)

## Routing Table

![](routing-table-10.png)

## Routing Table

![](routing-table-11.png)

## Routing Table

![](routing-table-12.png)

## Routing Table

-   Storing nodes this way allows us to convert the address space into a directed graph.
-   Lookups tend to be logarithmic.
-   Allows a traversal accross the entire address space by storing a lot of information about our neighbors, but allowing a wide distribution to "far-away" nodes.

# Put and Get

## Put Logic

![](put-logic-1.png)

## Put Logic

![](put-logic-2.png)

## Put Logic

![](put-logic-3.png)

## Put Logic

![](put-logic-4.png)

## Get Logic

![](get-logic-1.png)

## Get Logic

![](get-logic-2.png)

## Get Logic

![](get-logic-3.png)

## Get Logic

![](get-logic-4.png)

## Get Logic

![](get-logic-5.png)

## RPC

-   For node communication, we defined an RPC protocol living on top of JeroMQ

```{.clojure}

(defn receive-connection [context]
  (let [socket (.createSocket context SocketType/REP)]
    (.connect socket "inproc://workers")
    (while true
      (let [res (-> socket
                     (.recv 0)
                     (msg/unpack)
                     (req-handler)
                     (msg/pack))]
        (.send socket res)))))
```

## RPC

-   We define a multimethod to handle the dispatching of the message

```{.clojure}
(defmulti req-handler (fn [req] (get req :req-type)))
```

# Annoyances

## NATs

![](petar.png)

## Hole Punching

-   External computers don't know how to talk directly to computers within NAT (without a port forward)
    ![](nat-problem.png)

## Hole Punching

-   One solution, both computers connect to a server
    ![](nat-hole-punch-step-one.png)

## Hole Punching

-   Since socket is open to both computers, the server can stich together a direct connection.
    ![](nat-hole-punch-step-two.png)

## Hole Punching

-   In order to keep this decentralized, we use the following algorithm.
    -   Any node that has an open port gets priority on the routing table.
    -   We maintain one constant connection to a node with an open port per bucket.
    -   Each node will consistently publish any nodes that it has reference to as part of its metadata.
    -   When trying to "get" from a closed node, a hole-punch will be attempted through a mutually connected node if it exists.

# Addendum: CLJS from the Perspective of a JS Dev

## JavaScript

-   JavaScript **punishes** the OOP programmer.
-   Forced me to adopt functional programming techniques such as data/function seperation and composition.
-   Has significant gaps though:
    -   Large parts of the standard library focus around mutation
    -   Missing pieces that should be a part of the language but aren't
    -   "Pit of Despair" (classes, prototypical inheritence, null vs undefined)

## More Similar than Different

-   Surprising amount of isomorphisms between idiomatic JavaScript and idiomatic ClojureScript
-   Shared philosophy of working with many functions over relatively few data types
-   Both embrace dynamic programming.

## React vs Reagent

-   ![](factory-functions.png)

## JSX vs Hiccup

-   Always wished JSX used native JS Objects or Arrays
-   JSX brings the pain of HTML into JavaScript
-   Awkward escapement rules trip up newbies
-   Reagent's take on Hiccup not only fixes this, but brings in other nice things:
    -   Emmet-like expansion
    -   classnames-like functionality

## Redux vs Reframe

-   Removes need for secondary libraries to handle async, effects
-   Cuts down on boilerplate

## Future JavaScript today

-   Optional Chaining (https://github.com/tc39/proposal-optional-chaining)
-   Pattern Matching (https://github.com/tc39/proposal-pattern-matching)
-   Pipeline Operator (https://github.com/tc39/proposal-pipeline-operator)
-   Do Expressions (https://github.com/tc39/proposal-do-expressions)
-   Protocols (https://github.com/michaelficarra/proposal-first-class-protocols)
-   Macros (https://github.com/kentcdodds/babel-plugin-macros)

## Other Niceties

-   Immutability by default
-   Homoiconicity
-   One null type
-   No "bad parts"

# Further Reading!

## Links

\begin{columns}
\begin{column}{0.48\textwidth}
Kademlia Paper
\includegraphics[width=\linewidth]{qr-kad-paper.png}
\end{column}
\begin{column}{0.48\textwidth}
Feross Aboukhadijeh's talk
\includegraphics[width=\linewidth]{qr-kad-youtube.png}
\end{column}
\end{columns}

# Demo

## Contact

-   Thomas Gebert

    -   thomas@gebert.app
    -   gitlab.com/tombert

-   Nicholas Misturak
    -   gitlab.com/nrmisturak
